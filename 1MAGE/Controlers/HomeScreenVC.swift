//
//  HomeScreenVC.swift
//  1MAGE
//
//  Created by Mohit on 7/11/16.
//  Copyright © 2016 VikasSoni. All rights reserved.
//

import UIKit
import SafariServices
class HomeScreenVC: UIViewController,UIWebViewDelegate,SFSafariViewControllerDelegate,UIAlertViewDelegate,NSURLConnectionDelegate,UIImagePickerControllerDelegate  {
    var enteredURL = ""
    
    @IBOutlet weak var webViewCustom: UIWebView!
    
    //var vc = SFSafariViewController()
    var loaderShowed = false
    var window = appDelegate.window
    var validates = false
    var backButtonTap = true
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let homeButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: kbackIcon), style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        
        self.navigationItem.leftBarButtonItem = homeButton
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        
        
        if CommonClass.isConnectedToNetwork()
        {
            self.loadURL()
            
        }
        else
        {
            let alert = UIAlertController(title:k1mage , message: knetwrokError, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: kok, style: UIAlertAction.Style.cancel, handler: nil));
            
            self.present(alert, animated: true, completion: nil)
        }
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        //        var config : SwiftLoader.Config = SwiftLoader.Config()
        //        config.size = 100
        //        config.spinnerColor = .redColor()
        //        config.foregroundColor = .whiteColor()
        //        config.foregroundAlpha = 0.8
        webViewCustom.delegate = self
    }
    
    //    func goBack()
    //    {
    //        webViewCustom.goBack()
    //    }
    func loadURL()
    {
        if enteredURL != ""
        {
            //code comment
            //            let request = NSURLRequest(url: NSURL(string: enteredURL)! as URL)
            //            let connection = NSURLConnection(request: request as URLRequest, delegate: self)
            //
            //
            //                    let url = NSURL(string: enteredURL)
            //            let vc  = SFSafariViewController(url: url! as URL, entersReaderIfAvailable: true)
            //            vc.delegate = self
            //            self.navigationController?.present(vc, animated: false, completion: nil)
            //end of code comment
            
            let request = NSURLRequest(url: NSURL(string: enteredURL)! as URL)
            webViewCustom.loadRequest(request as URLRequest);
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    ////code comment
    //    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
    //        controller.dismiss(animated: true, completion: nil)
    //        back()
    //    }
    //
    
    
    @objc func back()
    {
        backButtonTap = true
        if(webViewCustom.canGoBack) {
            webViewCustom.goBack()
        }
//        backButtonTap = true
//        if(webViewCustom.canGoBack) {
//            webViewCustom.goBack()
//        }
//                if Defaults.value(forKey: reloaded) as! Bool == true
//                {
//                    let loginVC = kmainStoryBoard.instantiateViewController(withIdentifier: "loginVC")
//                    let loginNavigationController = UINavigationController()
//                    loginNavigationController.pushViewController(loginVC, animated: true)
//
//                    appDelegate.window!.rootViewController = loginNavigationController
        
             //   }
//
//                else
//                {
//                    backButtonTap = true
//                    if(webViewCustom.canGoBack) {
//                        webViewCustom.goBack()
//                    }
//                   // self.navigationController?.popViewController(animated: true)
//                }
    
    }
    
    
    
    
    //MARK: - Webview Delegates
    func webViewDidStartLoad(_ webView: UIWebView){
        if CommonClass.isConnectedToNetwork(){
            if !loaderShowed
            {
                // SwiftLoader.show("Loading", animated: true)
                customActivityIndicatory(self.view, startAnimate: true)
                loaderShowed = true
                
            }
        }
        else{
            let alert = UIAlertController(title:k1mage , message: knetwrokError, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: kok, style: UIAlertAction.Style.cancel, handler: nil));
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        // print(error?.description)
        
        if !validates
        {
            if error != nil
            {
                
                
                // SwiftLoader.hide()
                customActivityIndicatory(self.view, startAnimate: false)
                // let alertController = UIAlertController(title: k1mage, message: kincorrectURL, preferredStyle: .alert)
                //                        let alertController = UIAlertController(title: k1mage, message: kincorrectURL, preferredStyle: .alert)
                //                        // Create the actions
                //                        let okAction  = UIAlertAction(title: kok, style: UIAlertActionStyle.default, handler: { (alert) in
                //                            //  self.moveToLogin()
                //                        })
                //                        // Add the actions
                //                        alertController.addAction(okAction)
                //
                //                        // Present the controller
                //                        //self.presentViewController(alertController, animated: true, completion: nil)
                //                        self.present(alertController, animated: true, completion: nil)
                
                
                
                
            }
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        //  SwiftLoader.hide()
        customActivityIndicatory(self.view, startAnimate: false)
        // print()
    }
    
    
    func moveToLogin()
    {
        if Defaults.value(forKey: reloaded) as! Bool == true
        {
            let loginNavController = UINavigationController()
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: kloginVC)
            loginNavController.pushViewController(loginVC!, animated: true)
            self.window?.rootViewController = loginNavController
            
        }
            
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    /*  func connection(connection: NSURLConnection,
     didReceiveResponse response: URLResponse){
     if response is HTTPURLResponse{
     
     if (response as! HTTPURLResponse).statusCode != 200{
     let alertController = UIAlertController(title: k1mage, message: kincorrectURL, preferredStyle: .alert)
     
     // Create the actions
     let okAction = UIAlertAction(title: kok, style: UIAlertActionStyle.default) {
     UIAlertAction in
     self.moveToLogin()
     }
     
     // Add the actions
     alertController.addAction(okAction)
     
     // Present the controller
     self.present(alertController, animated: true, completion: nil)
     }
     else
     {
     validates = true
     print((response as! HTTPURLResponse).statusCode)
     // UIWebView.loadRequest(webViewCustom)(NSURLRequest(URL: NSURL(string:enteredURL)! as URL))
     
     }
     }
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @discardableResult
    func customActivityIndicatory(_ viewContainer: UIView, startAnimate:Bool? = true) -> UIActivityIndicatorView {
        let mainContainer: UIView = UIView(frame: viewContainer.frame)
        mainContainer.center = viewContainer.center
        mainContainer.backgroundColor = UIColor.white
        mainContainer.alpha = 0.5
        mainContainer.tag = 789456123
        mainContainer.isUserInteractionEnabled = false
        
        let viewBackgroundLoading: UIView = UIView(frame: CGRect(x:0,y: 0,width: 80,height: 80))
        viewBackgroundLoading.center = viewContainer.center
        
        viewBackgroundLoading.backgroundColor = UIColor.black
        viewBackgroundLoading.alpha = 0.5
        viewBackgroundLoading.clipsToBounds = true
        viewBackgroundLoading.layer.cornerRadius = 15
        
        let activityIndicatorView: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.frame = CGRect(x:0.0,y: 0.0,width: 40.0, height: 40.0)
        activityIndicatorView.style =
            UIActivityIndicatorView.Style.whiteLarge
        activityIndicatorView.center = CGPoint(x: viewBackgroundLoading.frame.size.width / 2, y: viewBackgroundLoading.frame.size.height / 2)
        if startAnimate!{
            viewBackgroundLoading.addSubview(activityIndicatorView)
            mainContainer.addSubview(viewBackgroundLoading)
            viewContainer.addSubview(mainContainer)
            activityIndicatorView.startAnimating()
        }else{
            for subview in viewContainer.subviews{
                if subview.tag == 789456123{
                    subview.removeFromSuperview()
                }
            }
        }
        return activityIndicatorView
    }
    
}
