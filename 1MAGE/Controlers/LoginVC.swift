//
//  ViewController.swift
//  1MAGE
//
//  Created by Mohit on 7/11/16.
//  Copyright © 2016 VikasSoni. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var urlTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false;
        urlTF.delegate = self
        urlTF.tag = 1000
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true

        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(_ animated: Bool) {
        Defaults.setValue(0, forKey: reloaded)
        //addLayeronTF()
        self.urlTF.setBottomBorder()
        addColoredPlaceHolder()
        if Defaults.value(forKey: kuserUrl) != nil{
            self.urlTF.text = Defaults.value(forKey: kuserUrl) as? String
        }

    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func addLayeronTF()
    {
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.black.cgColor
        border.frame = CGRect(x: 0, y: urlTF.frame.size.height - width, width:  urlTF.frame.size.width, height: urlTF.frame.size.height)
        
        border.borderWidth = width
        urlTF.layer.addSublayer(border)
        urlTF.layer.masksToBounds = true
    }
    
    
    func addColoredPlaceHolder()
    {
        urlTF.attributedPlaceholder = NSAttributedString(string:"Enter URL Here(http://www.example.com)",
                                                         attributes:[NSAttributedString.Key.foregroundColor:UIColor.init(patternImage: UIImage.init(named: "FontColor")!)])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func resetButtonClicked(sender: UIButton) {
        self.urlTF.text = ""

    }
    @IBAction func okButtonClicked(sender: UIButton) {
        
       GoButtonTap()
    }
    func GoButtonTap() {
        urlTF.resignFirstResponder()
      //  urlTF.text = "http://imageots.1mage.com:8080/v1amobile"//need to comment after testing
     
        if urlTF.text == ""
            
            
        {
            let alert = UIAlertController(title:k1mage , message: kemptyURL, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: kok, style: UIAlertAction.Style.cancel, handler: nil));
            
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            if self.validateURL(url: urlTF.text!)
            {
                if CommonClass.isConnectedToNetwork()
                {
                    Defaults.setValue(self.urlTF.text!, forKey: kuserUrl)
                    let homeVC = kmainStoryBoard.instantiateViewController(withIdentifier: kHomeVC) as! HomeScreenVC
                    homeVC.enteredURL = self.urlTF.text!
                    self.navigationController?.pushViewController(homeVC, animated: true)
                }
                else
                {
                    let alert = UIAlertController(title:k1mage , message: knetwrokError, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: kok, style: UIAlertAction.Style.cancel, handler: nil));
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
                
                
            }
            else
            {
                
                let alert = UIAlertController(title:k1mage , message: kincorrectURL, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: kok, style: UIAlertAction.Style.cancel, handler: nil));
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
    }
    
    
    
    //MARK: - TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1000
        {
          //  CommonClass().animateViewMoving(up: true, moveValue: 150, Scene: self)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1000
        {
            textField.resignFirstResponder()
          //  CommonClass().animateViewMoving(up: false, moveValue: 150, Scene: self)
        }
        GoButtonTap()
        return true
    }
    
    
    
    
    
    func validateURL(url:String) -> Bool
    {
        //let urlRegEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
      //let urlRegEx = "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
       // let urlRegEx = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
       // let urlRegEx = "^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\\-]*[A-Za-z0-9])$"
     // let urlRegEx = "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}"
  
      //  let urlRegEx = "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$"
        
//        let urltest = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
        let urlString = NSURL(string: url)
        
        return UIApplication.shared.canOpenURL(urlString! as URL)
    
        
    }
    
}

//64.15.187.205
extension UITextField {
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 2.0
        self.layer.shadowRadius = 0.0
    }
}
